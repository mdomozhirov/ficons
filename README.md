## Basic Usage

The quickest way to use these icons is to simply copy the source for the icon you need from [ficons.com](https://ficons.com) and inline it directly into your HTML:

```html
<svg class="h-6 w-6 text-gray-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path
    stroke-linecap="round"
    stroke-linejoin="round"
    stroke-width="2"
    d="M12 8v4l3 3m6-3a9 9 0 11-18 0 9 9 0 0118 0z"
  />
</svg>
```

Both icon styles are preconfigured to be stylable by setting the `color` CSS property, either manually or using utility classes like `text-gray-500` in a framework like [Tailwind CSS](https://tailwindcss.com).

## React

First, install `@domozhirov/ficons-react` from npm:

```sh
npm install @domozhirov/ficons-react
```

Now each icon can be imported individually as a React component:

```js
import { BeakerIcon } from '@domozhirov/ficons-react/outline'

function MyComponent() {
  return (
    <div>
      <BeakerIcon className="h-5 w-5 text-blue-500"/>
      <p>...</p>
    </div>
  )
}
```

The 24x24 outline icons can be imported from `@domozhirov/ficons-react/outline`.

Icons use an upper camel case naming convention and are always suffixed with the word `Icon`.

[Browse the full list of icon names on UNPKG &rarr;](https://unpkg.com/browse/@domozhirov/ficons-react/outline/)


## Vue

*Note that this library currently only supports Vue 3. For Vue 2 @domozhirov/ficons-vue2*

First, install `@domozhirov/ficons-vue3` from npm:

```sh
npm install @domozhirov/ficons-vue3
```

Now each icon can be imported individually as a Vue component:

```vue
<template>
  <div>
    <BeakerIcon class="h-5 w-5 text-blue-500"/>
    <p>...</p>
  </div>
</template>

<script>
import { BeakerIcon } from '@domozhirov/ficons-vue3/outline'

export default {
  components: { BeakerIcon }
}
</script>
```

The 24x24 outline icons can be imported from `@domozhirov/ficons-vue3/outline`.

Icons use an upper camel case naming convention and are always suffixed with the word `Icon`.

[Browse the full list of icon names on UNPKG &rarr;](https://unpkg.com/browse/@domozhirov/ficons-vue3/outline/)

## License

This library is MIT licensed.
